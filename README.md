# README #

SUMMARY:
  This repo is a POC for implementation of system stats monitoring via collectd -> logstash -> elasticsearch -> kibana

### How do I get set up? ###

* Pre-requisites:
  Following pre-requisites should be installed before starting with this repo.
  - VirtualBox
  - Vagrant

* Installation Steps:
  - Go to directory where this repo is kept.
  - run "vagrant up"
  - This will download a box from hashicorp atlas which contains pre installed ELK components.
  - The setup consists of 4 machines
     > One machine for kibana
     > 3 machines, each contains collectd, logstash and elasticsearch
  - Vagrant provisioning scripts will ensure all components are booted already
  - Open link http://192.168.17.99:5601 in your browser to access kibana
* How to run tests
  - Create a free memory usage visualization in Kibana
  - On any machine, run /vagrant/scripts/memload.pl <###> (### is the number of MB to load in memory)
