#!/bin/bash

#
# Adding host entries
#
sudo /vagrant/scripts/appendHosts.sh

#
# Configuring collectd
#
sudo mkdir -p /etc/collectd/;
sudo cp /vagrant/templates/collectd.conf.tmpl /etc/collectd/collectd.conf

#
# Variables for later use

HOST=$(hostname);
MACH_ID=$(echo $HOST | cut -d"." -f1 | tr -d [a-z]);
IP_SUFFIX=$(( 9 + $MACH_ID ));
case $MACH_ID in
    1 )
       UNICAST_HOSTS='\"192.168.17.11:9300\", \"192.168.17.12:9300\"' ;;
    2 )
       UNICAST_HOSTS='\"192.168.17.10:9300\", \"192.168.17.12:9300\"' ;;
    3 )
       UNICAST_HOSTS='\"192.168.17.11:9300\", \"192.168.17.10:9300\"' ;;
esac

#
# Configuring logstash
#
sudo mkdir -p /etc/logstash/conf.d/
sudo cp /vagrant/templates/logstash.conf.tmpl /etc/logstash/conf.d/logstash.conf

#
# Configuring elasticsearch, lots of corny stuff here needs to be improved later
#
sudo mkdir -p /vagrant/${HOST}/esdata /etc/elasticsearch/
sudo cp /vagrant/templates/elasticsearch.yml.tmpl /etc/elasticsearch/elasticsearch.yml

#
# Replacing hostname tokens in conf files
#

sudo sed -i "s|%HOST%|$HOST|g" /etc/collectd/collectd.conf /etc/elasticsearch/elasticsearch.yml

sudo sed -i "s|%IP_SUFFIX%|$IP_SUFFIX|g" /etc/elasticsearch/elasticsearch.yml /etc/collectd/collectd.conf

sudo sed -i "s|%UNICAST_HOSTS%|$UNICAST_HOSTS|g" /etc/elasticsearch/elasticsearch.yml
sudo service elasticsearch start
#
# Setting up collectd as a service
#
sudo cp /vagrant/templates/init.d-collectd /etc/init.d/collectd
sudo chmod 755 /etc/init.d/collectd
sudo ln -s /opt/collectd/sbin/collectd /usr/sbin/collectd
sudo ln -s /opt/collectd/sbin/collectdmon /usr/sbin/collectdmon
sudo /etc/init.d/collectd start
sudo chkconfig --levels 235 collectd on

